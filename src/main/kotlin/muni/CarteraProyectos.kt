package muni

interface CarteraProyectos {
    val nombre: String
    fun agregarProyecto(p: Proyecto): Boolean
    fun obtenerProyectoPrioritario(): Proyecto?
}