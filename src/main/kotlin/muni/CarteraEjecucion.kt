package muni

class CarteraEjecucion(override val nombre: String) : CarteraProyectos {
    private val proyectos: MutableList<Proyecto> = mutableListOf()

    override fun agregarProyecto(p: Proyecto): Boolean {
        for(pc : Proyecto in proyectos) {
            if(pc.id == p.id)
                return false
        }

        proyectos.add(p)
        return true
    }

    override fun obtenerProyectoPrioritario(): Proyecto? {
        if(proyectos.isEmpty())
            return null

        var menor = proyectos[0]

        for(p : Proyecto in proyectos) {
            if(p.diasEjec < menor.diasEjec)
                menor = p
        }

        return menor
    }
}