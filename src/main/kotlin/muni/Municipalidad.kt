package muni

class Municipalidad (val nombre: String) {
    private val carteras: MutableList<CarteraProyectos> = mutableListOf()

    fun agregarCartera(c: CarteraProyectos) : Boolean {
        for(cCheck: CarteraProyectos in carteras) {
            if(cCheck.nombre == c.nombre)
                return false
        }

        carteras.add(c)
        return true
    }

    private fun obtenerCartera(nombreCartera: String) : CarteraProyectos? {
        for(cCheck: CarteraProyectos in carteras) {
            if(cCheck.nombre == nombreCartera)
                return cCheck
        }

        return null
    }

    fun agregarProyectoACartera(p: Proyecto, nombreCartera: String) : Boolean {
        val cartera = obtenerCartera(nombreCartera) ?: return false
        return cartera.agregarProyecto(p);
    }

    fun obtenerProyectoPriorizadoCartera(nombreCartera: String) : Proyecto? {
        val cartera = obtenerCartera(nombreCartera) ?: return null
        return cartera.obtenerProyectoPrioritario()
    }
}