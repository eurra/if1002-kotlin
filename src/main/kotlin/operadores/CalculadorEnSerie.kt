package operadores

class CalculadorEnSerie {
    private val lista: MutableList<OperadorBinario> = mutableListOf()

    fun agregarOperador(op: OperadorBinario) {
        lista.add(op)
    }

    fun calcularEnSerie(numInicial: Int) : Int {
        var res = numInicial

        for(o in lista) {
            res = o.calcular(res)
        }

        return res
    }
}