package operadores

fun main() {
    val calc = CalculadorEnSerie()
    calc.agregarOperador(Suma(5))
    calc.agregarOperador(Suma(10))
    calc.agregarOperador(Resta(1))

    println("Ingrese número inicial:")
    val num = readLine()?.toInt() ?: 1
    println("Resultado calculador en serie: ${calc.calcularEnSerie(num)}")
}