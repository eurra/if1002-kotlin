package operadores

class Resta(num1: Int) : OperadorBinario(num1) {
    override fun calcular(num2: Int): Int {
        return this.num1 - num2
    }
}