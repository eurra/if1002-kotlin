package operadores

abstract class OperadorBinario(val num1: Int) {
    abstract fun calcular(num2: Int) : Int
}