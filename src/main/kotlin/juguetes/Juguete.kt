package juguetes

abstract class Juguete(val id: Int) {
    protected val estadoEnsamble = false
    protected val piezas = mutableListOf<Pieza>()

    abstract fun ensamblar() : Boolean

    fun estaEnsamblado() : Boolean {
        return estadoEnsamble
    }

    // Esto se puede ajustar para colocar algún tipo de validación
    // al tratar de agregar la pieza
    fun agregarPieza(p: Pieza) : Boolean {
        piezas.add(p)
        return true
    }
}