package juguetes

enum class Material {
    MADERA,
    METAL,
    PLASTICO
}

class Pieza(val tipo: String, val material: Material)