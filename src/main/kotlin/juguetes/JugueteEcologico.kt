package juguetes

class JugueteEcologico(id: Int, val cantMinMadera: Int) : Juguete(id) {

    // Revisa si tiene al menos 'cantMinMadera' piezas de madera
    override fun ensamblar(): Boolean {
        var cuenta = 0

        for(p in piezas) {
            if(p.material == Material.MADERA)
                cuenta++
        }

        if(cuenta >= cantMinMadera)
            return true

        return false
    }
}