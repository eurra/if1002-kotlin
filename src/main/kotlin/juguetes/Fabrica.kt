package juguetes

class Fabrica {
    private val juguetes = mutableListOf<Juguete>()

    // Punto 1
    fun agregarJuguete(j: Juguete) : Boolean {
        if(buscarJuguete(j.id) != null)
            return false

        juguetes.add(j)
        return true
    }

    // Punto 2
    fun agregarPiezaAJuguete(p: Pieza, id: Int) : Boolean {
        val j = buscarJuguete(id) ?: return false
        return j.agregarPieza(p)
    }

    // Punto 3
    fun ensamblarJuguete(id: Int) : Boolean {
        val j = buscarJuguete(id) ?: return false

        if(j.estaEnsamblado())
            return false

        return j.ensamblar()
    }

    // Utilitario
    private fun buscarJuguete(id: Int) : Juguete? {
        for(j in juguetes) {
            if(j.id == id)
                return j
        }

        return null
    }
}