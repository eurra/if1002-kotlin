package juguetes

class JugueteMixto(id: Int) : Juguete(id) {

    // Revisión de piezas con material repetido
    // No es el método más eficiente, pero funciona
    override fun ensamblar(): Boolean {
        for(i in 0..piezas.size) {
            for(j in i + 1..piezas.size)
                if(piezas[i].material == piezas[j].material)
                    return false
        }

        return true
    }
}