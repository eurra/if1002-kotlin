package guerraNaval

enum class Direccion {
    NORTE,
    SUR,
    ESTE,
    OESTE
}