package guerraNaval

const val anchoDef : Int = 5
const val altoDef : Int = 5

class CreadorTablero() {
    private var nombre: String = "Tablero sin nombre"
    private var ancho: Int = anchoDef
    private var alto: Int = altoDef

    fun configurarAncho(ancho: Int) {
        this.ancho = ancho
    }

    fun configurarAlto(alto: Int) {
        this.alto = alto
    }

    fun crearTablero() : Tablero {
        val ancho = if(this.ancho < anchoDef) {
            anchoDef
        } else {
            this.ancho
        }

        val alto = if(this.alto < anchoDef) {
            altoDef
        } else {
            this.alto
        }

        return Tablero(nombre, ancho, alto)
    }
}