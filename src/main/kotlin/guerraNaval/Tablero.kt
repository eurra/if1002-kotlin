package guerraNaval

class Tablero {
    val nombre: String
    val ancho: Int
    val alto: Int
    private val tablero : MutableMap<Coordenada, Barco>

    internal constructor(nombre: String, ancho: Int, alto: Int) {
        this.nombre = nombre
        this.ancho = ancho
        this.alto = alto
        this.tablero = mutableMapOf()
    }

    fun agregarBarco(b: Barco, c: Coordenada) : Resultado {
        // Check 1: ¿La coordenada es válida?
        val r = c.validaEnTablero(this).let {
            if(it.error)
                return it
        }

        // Check 2: ¿El barco está dentro del tablero?
        val d = b.direccion
        val l = b.largo

        val dif = when(d) {
            Direccion.ESTE -> ancho - (c.x + l)
            Direccion.OESTE -> c.x - l
            Direccion.SUR -> alto - (c.y + l)
            Direccion.NORTE -> c.y - l
        }

        if(dif < 0)
            return Resultado("El barco especificado se sale del tablero", true)

        // Check 3: ¿El barco choca con otro barco ya agregado?
        val barcoNuevoIt = b.iterar(c)

        while(barcoNuevoIt.hasNext()) {
            val nuevoCheck = barcoNuevoIt.next()

            for((oc, ob) in tablero) {
                val otroBarcoIt = ob.iterar(oc)

                while(otroBarcoIt.hasNext()) {
                    val otroCheck = otroBarcoIt.next()

                    if(nuevoCheck == otroCheck)
                        return Resultado("El barco colisiona con otro agregado antes", true)
                }
            }
        }

        // Sin problemas para agregar
        tablero[c.copy()] = b
        return Resultado()
    }
}