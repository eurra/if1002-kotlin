package guerraNaval

data class Coordenada(val x: Int, val y: Int) {
    fun validaEnTablero(t: Tablero) : Resultado {
        if (x < 1 || x > t.ancho)
            return Resultado("La coordenada x está fuera de rango del tablero", true)

        if (y < 1 || y > t.alto)
            return Resultado("La coordenada y está fuera de rango del tablero", true)

        return Resultado()
    }
}