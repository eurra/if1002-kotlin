package guerraNaval

class Barco {
    val nombre: String
    val largo: Int
    val direccion: Direccion

    constructor(largo: Int, direccion: Direccion, nombre: String = "S/N") {
        this.largo = if(largo < 1) {
            1
        }
        else {
            largo
        }

        this.direccion = direccion
        this.nombre = nombre
    }

    fun iterar(coordIni: Coordenada): Iterator<Coordenada> {
        var coordActual = coordIni.copy()
        var cuenta = 0

        return object : Iterator<Coordenada> {
            override fun hasNext(): Boolean {
                return cuenta <= largo
            }

            override fun next(): Coordenada {
                if(hasNext()) {
                    cuenta++

                    coordActual = when (direccion) {
                        Direccion.NORTE -> coordActual.copy(coordActual.x, coordActual.y - 1)
                        Direccion.SUR -> coordActual.copy(coordActual.x, coordActual.y + 1)
                        Direccion.OESTE -> coordActual.copy(coordActual.x - 1, coordActual.y)
                        Direccion.ESTE -> coordActual.copy(coordActual.x + 1, coordActual.y)
                    }
                }

                return coordActual
            }
        }
    }
}