package banco

enum class Resultado(val msj: String) {
    EXITO("Operación exitosa."),
    EXISTE_CUENTA("La cuenta especificada ya existe en banco."),
    NO_EXISTE_CUENTA("La cuenta especificada no existe en el banco."),
    SALDO_INSUFICIENTE("La cuenta especificada no tiene saldo suficiente.")
}

class Banco(val nombre: String) {
    private val cuentas: MutableList<CuentaBancaria> = mutableListOf()

    // Punto 1
    fun agregarNuevaCuenta(rut: String, nombre: String, edad: Int) : Resultado {
        val c: CuentaBancaria? = buscarCuenta(rut)

        if(c != null)
            return Resultado.EXISTE_CUENTA

        val nuevaCuenta = CuentaBancaria(rut, nombre, edad)
        cuentas.add(nuevaCuenta)
        return Resultado.EXITO
    }

    // Punto 2
    fun quitarCuenta(rut: String) : Resultado {
        val c: CuentaBancaria = buscarCuenta(rut) ?: return Resultado.NO_EXISTE_CUENTA

        cuentas.remove(c)
        return Resultado.EXITO
    }

    // Punto 3
    fun depositarEnCuenta(rut: String, monto: Int) : Resultado {
        val c: CuentaBancaria = buscarCuenta(rut) ?: return Resultado.NO_EXISTE_CUENTA

        c.depositar(monto)
        return Resultado.EXITO
    }

    // Punto 4
    fun girarDeCuenta(rut: String, monto: Int) : Resultado {
        val c: CuentaBancaria = buscarCuenta(rut) ?: return Resultado.NO_EXISTE_CUENTA

        if(!c.girar(monto))
            return Resultado.SALDO_INSUFICIENTE
        else
            return Resultado.EXITO
    }

    // Punto 5
    fun obtenerPromedioSaldoCuentasMenores30() : Double {
        var total = 0

        if(cuentas.size == 0)
            return 0.0

        for(c in cuentas) {
            if(c.edad < 30)
                total += c.obtenerSaldo()
        }

        return total.toDouble() / cuentas.size
    }

    private fun buscarCuenta(rut: String) : CuentaBancaria? {
        for(c in cuentas) {
            if(c.rut == rut) {
                return c
            }
        }

        return null
    }
}