package banco

class CuentaBancaria(val rut: String, val nombre: String, val edad: Int) {
    private var saldo: Int = 0

    fun depositar(monto: Int) : Boolean {
        if(monto < 0)
            return false

        this.saldo += monto
        return true
    }

    fun girar(monto: Int) : Boolean {
        if(monto > this.saldo)
            return false

        this.saldo -= monto
        return true
    }

    fun obtenerSaldo() : Int {
        return this.saldo
    }
}