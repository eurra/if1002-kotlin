package banco



fun main() {
    println("Bienvenido a la gestión de cuentas bancarias!")
    println("Ingrese nombre de su banco:")

    val nombreBanco = readLine() ?: "Banco"
    val b = Banco(nombreBanco)
    var opcion: Int

    do {
        println("Ingrese opción a ejecutar:")
        println("1 - Agregar una cuenta nueva.")
        println("2 - Quitar una cuenta existente.")
        println("3 - Depositar dinero en una cuenta.")
        println("4 - Girar dinero de una cuenta.")
        println("5 - Obtener promedio de saldos de cuentas asociadas a menores de 30 años.")
        println("6 - Salir.")

        opcion = (readLine() ?: "6").toInt()

        when(opcion) {
            // Punto 1
            1 -> {
                println("Ingrese RUT:")
                val rut = readLine() ?: run {
                    println("RUT inválido")
                    null
                } ?: continue

                println("Ingrese nombre:")
                val nombre = readLine() ?: run {
                    println("Nombre inválido")
                    null
                } ?: continue

                println("Ingrese edad:")
                val edad = (readLine() ?: run {
                    println("Edad inválida")
                    null
                } ?: continue).toInt()

                println(b.agregarNuevaCuenta(rut, nombre, edad).msj)
            }

            // Punto 2
            2 -> {
                println("Ingrese RUT:")
                val rut = readLine() ?: run {
                    println("RUT inválido")
                    null
                } ?: continue

                println(b.quitarCuenta(rut).msj)
            }

            // Punto 3
            3 -> {
                println("Ingrese RUT:")
                val rut = readLine() ?: run {
                    println("RUT inválido")
                    null
                } ?: continue

                println("Ingrese monto a depositar:")
                val monto = (readLine() ?: run {
                    println("Monto inválido")
                    null
                } ?: continue).toInt()

                println(b.depositarEnCuenta(rut, monto).msj)
            }

            // Punto 4
            4 -> {
                println("Ingrese RUT:")
                val rut = readLine() ?: run {
                    println("RUT inválido")
                    null
                } ?: continue

                println("Ingrese monto a girar:")
                val monto = (readLine() ?: run {
                    println("Monto inválido")
                    null
                } ?: continue).toInt()

                println(b.girarDeCuenta(rut, monto).msj)
            }

            // Punto 5
            5 -> {
                val prom = b.obtenerPromedioSaldoCuentasMenores30()
                println("El promedio del saldo de los clientes menores de 30 años es $prom.")
            }

            6 -> continue
            else -> println("Opción inválida")
        }
    }
    while(opcion != 6)

    println("Adios!")
}