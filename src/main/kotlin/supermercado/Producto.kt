package supermercado

class Producto (
    val sku: Int,
    val etiqueta: String,
    val nombreProveedor: String,
    val precio: Int,
    val categoria: String,
    var stock: Int = 0
) {
    fun agregarStock(cantidad: Int) : Boolean {
        if(cantidad < 0)
            return false

        stock += cantidad;
        return true
    }
}