package supermercado

class Supermercado(val nombre: String) {
    private val productos: MutableList<Producto> = mutableListOf()

    fun agregarProducto(prod: Producto): Boolean {
        for(p in productos) {
            if(p.sku == prod.sku)
                return false
        }

        productos.add(prod)
        return true
    }
}