package busqueda

abstract class AlgoritmoBusqueda {
    private val elementos = mutableListOf<Int>()
    private var resultado: Int = -1

    fun agregarElemento(e: Int) {
        elementos.add(e)
    }

    fun buscar(elem: Int) {
        resultado = buscarElemento(elem, elementos)
    }

    fun imprimirResultado() : String {
        return "Elementos: ${elementos.toString()}\nPosición resultado: $resultado\n"
    }

    // Debe retornar la posición del texto buscado
    abstract fun buscarElemento(elemBuscado:Int, listaElementos: List<Int>) : Int
}