package busqueda

// Cambiar este bucador por el que ud. implemente
val buscador : AlgoritmoBusqueda = BuscadorDummy()

fun main() {
    println("***************************************************")
    println("***** Clase de prueba para algoritmo buscador *****")
    println("***************************************************")

    buscador.agregarElemento(5)
    buscador.agregarElemento(7)
    buscador.agregarElemento(1)
    buscador.agregarElemento(9)
    buscador.agregarElemento(10)

    buscador.buscar(5)
    println(buscador.imprimirResultado())

    buscador.buscar(9)
    println(buscador.imprimirResultado())

    buscador.buscar(2)
    println(buscador.imprimirResultado())
}