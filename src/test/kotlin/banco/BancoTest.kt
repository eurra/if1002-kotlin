package banco

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class BancoTest {

    @Test
    fun prueba() {
        val miBanco = Banco("BCI")

        assertEquals(Resultado.NO_EXISTE_CUENTA, miBanco.depositarEnCuenta("1234", 100))
        assertEquals(Resultado.EXITO, miBanco.agregarNuevaCuenta("1234", "Juanito", 20))
        assertEquals(Resultado.EXISTE_CUENTA, miBanco.agregarNuevaCuenta("1234", "Pepe", 25))
        assertEquals(Resultado.EXITO, miBanco.agregarNuevaCuenta("5678", "Pepe", 25))
        assertEquals(Resultado.EXITO, miBanco.depositarEnCuenta("5678", 300))
    }
}