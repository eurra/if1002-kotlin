package muni

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MunicipalidadTest {

    @Test
    fun general() {
        val p1 = Proyecto(1, "Proyecto 1", 25, 50000000.0)
        val p2 = Proyecto(2, "Proyecto 1", 30, 100000.0)
        val p3 = Proyecto(3, "Proyecto 1", 60, 2500000.0)
        val p4 = Proyecto(4, "Proyecto 1", 20, 30000000.0)

        val muni = Municipalidad("Coyhaique")

        val cartera1 = CarteraCosto("costo-proyectos")
        val cartera2 = CarteraEjecucion("tiempo-proyectos")
        assertEquals(true, muni.agregarCartera(cartera1))
        assertEquals(true, muni.agregarCartera(cartera2))
        assertEquals(false, muni.agregarCartera(cartera1))

        assertEquals(false, muni.agregarProyectoACartera(p1, "costo.proyectos"))
        assertEquals(true, muni.agregarProyectoACartera(p1, "costo-proyectos"))
        assertEquals(false, muni.agregarProyectoACartera(p1, "costo-proyectos"))
        muni.agregarProyectoACartera(p2, "costo-proyectos")
        muni.agregarProyectoACartera(p3, "costo-proyectos")
        muni.agregarProyectoACartera(p4, "costo-proyectos")

        muni.agregarProyectoACartera(p1, "tiempo-proyectos")
        muni.agregarProyectoACartera(p2, "tiempo-proyectos")
        muni.agregarProyectoACartera(p3, "tiempo-proyectos")
        muni.agregarProyectoACartera(p4, "tiempo-proyectos")

        assertEquals(p2, muni.obtenerProyectoPriorizadoCartera("costo-proyectos"))
        assertEquals(p4, muni.obtenerProyectoPriorizadoCartera("tiempo-proyectos"))
    }
}