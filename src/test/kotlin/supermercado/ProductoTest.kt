package supermercado

import org.junit.jupiter.api.Assertions.*
import kotlin.test.Test

internal class ProductoTest {

    @Test
    fun testBasicoProducto() {
        val superMercado = Supermercado("Hiperpatagonico")
        val prod1 = Producto(1, "manzana", "X", 200, "Frutas")
        val prod2 = Producto(2, "Cepillo de dientes", "Y", 1000, "Utiles de aseo", 100)

        println("Stock del producto '${prod1.etiqueta}': ${prod1.stock}")
        println("Stock del producto '${prod2.etiqueta}': ${prod2.stock}")

        println("Agregando 30 de stock al producto '${prod1.etiqueta}':")
        println(prod1.agregarStock(30))

        println("Agregando -10 de stock al producto '${prod1.etiqueta}':")
        println(prod1.agregarStock(-10))

        println("Agregando producto '${prod1.etiqueta}' al supermercado '${superMercado.nombre}'")
        println(superMercado.agregarProducto(prod1))

        println("Agregando producto '${prod2.etiqueta}' al supermercado '${superMercado.nombre}'")
        println(superMercado.agregarProducto(prod2))

        println("Agregando nuevamente producto '${prod1.etiqueta}' al supermercado '${superMercado.nombre}'")
        println(superMercado.agregarProducto(prod1))
    }
}