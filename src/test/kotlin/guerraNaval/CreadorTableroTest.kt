package guerraNaval

import org.junit.jupiter.api.Assertions.*
import kotlin.test.Test

internal class CreadorTableroTest {

    @Test
    fun general() {
        val ct = CreadorTablero()
        ct.configurarAlto(18)
        ct.configurarAncho(10)

        var t = ct.crearTablero()
        val b1 = Barco(3, Direccion.ESTE, "b1")
        val b2 = Barco(30, Direccion.NORTE, "b2")
        val b3 = Barco(5, Direccion.SUR, "b3")
        val b4 = Barco(6, Direccion.SUR, "b3")


        t.agregarBarco(b1, Coordenada(-1, 1)).let {
            assertEquals("La coordenada x está fuera de rango del tablero", it.mensaje)
            assertEquals(true, it.error)
        }

        t.agregarBarco(b1, Coordenada(3, 2)).let {
            assertEquals("Ok", it.mensaje)
        }

        t.agregarBarco(b2, Coordenada(1, 1)).let {
            assertEquals("El barco especificado se sale del tablero", it.mensaje)
            assertEquals(true, it.error)
        }

        t.agregarBarco(b3, Coordenada(4, 1)).let {
            assertEquals("El barco colisiona con otro agregado antes", it.mensaje)
            assertEquals(true, it.error)
        }

        t.agregarBarco(b4, Coordenada(7, 2)).let {
            assertEquals("Ok", it.mensaje)
        }
    }
}